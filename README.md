docker-u14silverstripe
===========

Development machine for Silverstripe Websites.

Important:
----------

There is no mySQL Server - because im am running mySQL on my Host Machine.
Mapping /var/lib/mysql to the guest is not comfortable to me.


Don't use it in product environment.

# Usage

    docker run -d --name=u14silverstripe \
      -v /path/to/www/:/var/www/ \
      -p port_of_nginx:80 \
      derrobert/docker-u14silverstripe:latest

# Detail

## SSH
We don't support SSH right now. You can use `docker exec` to enter the docker container.

docker exec -i -t [ID] bash
